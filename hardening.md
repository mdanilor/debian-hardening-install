TODO:

- [x] firejail
- [x] usbguard
- [x] ufw
- [x] sysctl, copiar do privacy guide do arch?
- [x] FDE
- [x] Network manager random mac
- [ ] evil maid?
- [ ] https upgrades? tor?
- [ ] lockdown bios and grub
- [x] hardened firefox
- [ ] diceware and secure passwords -> install
- [ ] keepassxc, falar que nao eh seguro usar totp. 
- [ ] traduzir pt br!
- [x] disable services and unsafe (accounts daemon)
- [x] cmdline privacy guide arch
- [x] apparmor mais perfis e notification
- [ ] firefox hardening
- [ ] other link
- [ ] install part
- [ ] acklolagements 
    + https://theprivacyguide1.github.io/linux_hardening_guide.html


# Debian install and hardening

Portugues: siga o guia em portugues aqui: 

If you haven't installed Debian yet, follow the install part of this guide

While installing, **encrypting your hard drive WITH A SECURE PASSPHSASE is highly encouraged**


## package updates

Keeping your system updated is essential, the updates patch vulnerabilities.

Install pk-update-icon to notify you if you have any updates:

> sudo apt install pk-update-icon

then run set gpk-update-viewer as command to install updates, to do that run:

> package-update-indicator-prefs

set  **gpk-update-viewer** as Update installer command and change refresh cache to **every hour** 

For **servers search for unattended-upgrades.**

## Install micro-code to mitigate spectre/meltdown 

Make sure that you have **contrib non-free** enabled on your source.list (if not check the install guide), then:

> sudo apt install intel-microcode

(for intel CPUs)

and for amd CPUs, run:

> sudo apt install amd64-microcode

now we are going to check if you are truly protected against spectre, reboot your machine, then

```
curl -L https://meltdown.ovh -o spectre-meltdown-checker.sh
chmod +x spectre-meltdown-checker.sh
sudo ./spectre-meltdown-checker.sh
```

And make sure that in the summary it's all green (ok)


## USBGuard to prevent badUSB (rubber ducky) attacks

badUSB (aka rubber ducky) attacks are just injecting keystrokes into the victim computer, so if the victim leaves the computer unattended, the attacker may put a malicious device in the USB port so it acts like a keyboard that types really fast injecting some malware into the victim's computer

to prevent that we are going to install usbguard, usbguard is a daemon that only allows the devices you previously allowed to be connected, so when a malicious USB device is plugged into the usb port it won't have any affect.

First install usbguard and its GUI:

> sudo apt install usbguard usbguard-applet-qt

Now unplug every device that is plugged in your USB ports, and run:

> sudo usbguard generate-policy > rules.conf

this will generate the rules containing the list of the device which are allowed (that's why I asked to disconnected all the USB devices before) 

Now install the rules:

> sudo install -m 0600 -o root -g root rules.conf /etc/usbguard/rules.conf

One last thing, edit the `/etc/usbguard/usbguard-daemon.conf` file and append your username at the IPCAllowedUsers and IPCAllowedGroup , like this `IPCAllowedGroups=root <username>`

Now enable and start usbguard daemon:

```
systemctl enable usbguard
systemctl restart usbguard
```

Now `usbguard-applet-qt` should start automatically when your system boots, but when you want to allow a device, run it, and a window will appear asking if you want to allow or block that device

## Firejail

Firejail prevents possibly vulnerable software to compromise your computer, it sandboxes your programs and limit what those programs can see and do.

To install just:

> sudo apt install firejail firejail-profiles firetools

If you want a more recent version install it from the backports:

> sudo apt -t buster-backports firejail firejail-profiles firetools

Now we are going to configure it:

Create firejail config folder

> mkdir -p ~/.config/firejail/

Now create globals.local file

```
sensible-editor ~/.config/firejail/globals.local
then insert the following:

net none
apparmor
```

This will disable network (internet) for all the programs, except those we explicitly allow, and apparmor.

But we want/need network from browsers from instance, also since browsers are a huge attack surface, we will create a "private" /home only for the browsers so it wont be able to access for instance your Desktop

```
mkdir ~/browsersHome/
sensible-editor ~/.config/firejail/firefox.local
then insert the following:

ignore net
private ~/browsersHome/

```

This will ignore the globals.local net none (allowing network) and point firefox's /home to ~/browsersHome/

Last configuration needed, if you are using a U2F security key (like yubikey), allow it:

> sudo sensible-editor /etc/firejail/firejail.config

and uncomment browser-disable-u2f and change it to no , like this: `browser-disable-u2f no`

Finally we are going to enable firejail to all the programs in your system, **if you install any new programs, run this again**

```
sudo firecfg
sudo firecfg --fix-sound
```

This will sandbox all programs automatically, now be aware that we disabled network for all the programs, so if wget or curl or ssh is not working remember to allow it like we did it with firefox, so for instance if you want to allow network for ssh, run:

> sensible-editor ~/.config/firejail/ssh.local

and insert `ignore net`

lastly there's a nice gui to show the sandboxes, open right click on the panel and select tools, or run it directly:

> /usr/lib/firetools/fstats

Debian (at least stable) doesn't ship the lastest firefox, only the esr (extended support release), so to sandbox the lastest version of firefox just follow the [guide](https://github.com/netblue30/firejail/wiki/Sandboxing-Binary-Software#mozilla-firefox-opt-install) (basically download the firefox archive and extract to /opt)

## MAC randomization

The physical address or the MAC address uniquely identifies you in your network, but mainly on Wireless networks it can be used to track you around and by doing this infer where you been.

In Debian most likely you are using networkmanager, to enable MAC randomization, run:

```
sudo sensible-editor /etc/NetworkManager/conf.d/30-mac-randomization.conf
and insert the following:

[device-mac-randomization]
# "yes" is already the default for scanning
wifi.scan-rand-mac-address=yes

[connection-mac-randomization]
ethernet.cloned-mac-address=random
wifi.cloned-mac-address=random

```

Done! You can change on per-network basis what is the desired behaviour, look at this [explanation](https://blogs.gnome.org/thaller/2016/08/26/mac-address-spoofing-in-networkmanager-1-4-0/) to understand what you need. For instance if you connect to a captive portal network (thoose you need to login with a account (or social media)) if you dont to login back in every time you connect to that network, change to **stable** on that network.


## Enable firewall (ufw)

If for instance your machine gets compromised if it doesn't have a firewall  malicious programs can open ports in your machine allowing unwanted remote access.

Now we are going to install and enable ufw:

```
sudo apt install ufw
sudo ufw default deny incoming 
sudo ufw enable
```

This way the firewall will block any incoming connections (wont allow to any ports to be open), but if you are running a server and need to allow clients connecting to your machine run: `sudo ufw allow http` (equivalent to `sudo ufw allow 80`) to allow a http server, to block it back `sudo ufw delete allow http` (deletes the rule that allows port 80) , if you only want to allow some ips to connect to your server: `sudo ufw allow from 192.168.15.7 to any port 80` (it will allow the client 192.168.15.7 to connect to port 80).

For ssh I recommend instead of allowing it, rate limit it! so if a unauthorized client is brute-forcing the passwords to try to get it, it will get blocked for some minutes. So run: `sudo ufw limit ssh`

## sysctl settings

Sysctl settings are run-time kernel configuration, these settings are only from [Tails](https://tails.boum.org) , you can access them [here](https://git-tails.immerda.ch/tails/plain/config/chroot_local-includes/etc/sysctl.d/)


```
sudo sensible-editor /etc/sysctl.d/tails.conf
and insert the following:

net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.all.disable_ipv6 = 1

net.ipv6.conf.lo.disable_ipv6 = 0
fs.protected_fifos = 2
fs.protected_regular = 2
kernel.kexec_load_disabled = 1
kernel.kptr_restrict=2
vm.mmap_rnd_bits=32
vm.mmap_rnd_compat_bits=16
net.ipv4.tcp_mtu_probing=1
kernel.yama.ptrace_scope=2
net.ipv4.tcp_timestamps=0
kernel.unprivileged_bpf_disabled=1

# some chromium based browsers (use brave browser!)
# need to create name-spaces, so uncomment the line below:
#kernel.unprivileged_userns_clone=1

# default anyway
kernel.dmesg_restrict = 1
```

## Apparmor

AppArmor is a Mandatory Access Control (MAC) system to confine programs to limited set of resources.

Appamor is installed and enabled by default in Debian 10 (Buster) , but we can install more profiles to confine more programs and enable appamor notification to let users know if a apparmor prevent a program to access a resource


```
sudo apt install apparmor-profiles apparmor-profiles-extras apparmor-notify
sudo usermod -aG adm $USER
```

To see if apparmor is running and which programs are confined, run:

> sudo aa-status

Firejail has a apparmor profile so confining even more the jailed programs

## Boot parameters

These parameters are passed to the kernel during boot.

They harden the memory management and cpu features to prevent Spectre/Meltdown, they came from [tails -- kernel hardening](https://tails.boum.org/contribute/design/kernel_hardening/)

To add the parameters run:

```
sudo sensible-editor /etc/default/grub
and append to GRUB_CMDLINE_LINUX_DEFAULT=

slab_nomerge slub_debug=FZP mce=0 page_poison=1 pti=on mds=full,nosmt module.sig_enforce=1 vsyscall=none 

```

Mine look like this: `GRUB_CMDLINE_LINUX_DEFAULT="quiet slab_nomerge slub_debug=FZP mce=0 page_poison=1 pti=on mds=full,nosmt module.sig_enforce=1 vsyscall=none"`


## Disable possible unsafe services

Systemd has been the default init system for most linux distros, a init system handles the services that are enabled and start them.

To list enabled services run:

> systemctl list-unit-files --type=service | grep enabled

Of the listed services, the services below are potentially unsafe:

* accounts-daemon : it is part of AccountService which allow programs to manipulate user account info (be aware that one time on cinnamon after disabling broke the system).
* bluetooth : unless you need bluetooth you should disable.

Of course there are some services that are not essential, they are:

* avahi-daemon: should help to locate for instance network printers
* ModemManager: if you use onboard modems (most likely not)
* pppd-dns : if you use dial-up internet (most likely not)

How to disable a service? You can only disable so it wont get started, but if another service needs it it will , in that case you can mask , completely avoid that service running, run:

> sudo systemctl mask accounts-daemon

and for disabling:

> sudo systemctl disable bluetooth


---

# Português: Instalação e endurecimento (tornar mais seguro) de Debian 

Se ainda não instalou o debian siga o guia de instalação

Eh altamente recomendado **instalar com disco criptofragado com uma frase-senha secura**

Confira também o guia de endurecimento do firefox.

## Atualizações de pacotes

Manter seu sistema atualizado é essencial, pois as atualizações corrigem vulnerabilidades que podem comprometer sua maquina.

Instale o pacote `pk-update-icon` para notificar se você tem que fazer alguma atualização:

> sudo apt install pg-update-icon

então configure  `gpk-update-viewer` como o comando de atualizar atualizações , para fazer isso, execute:

> package-update-indicator-prefs

e configure para **gpk-update-viewer** como comando de atualizar atualizações e mude para atualizar o cache para **cada hora**.

Para **servidores procure por unattended-upgrades.**

## Instalar micro-code para mitigar Spectre/Meltdown:

Tenha certeza que tem o **contrib e non-free** habilitados na sua source.list (se nao esta confira no guia de instalação aqui como fazer)

**Para CPUs da intel**

> sudo apt install intel-microcode

**Para CPUs da AMD**

> sudo apt install amd64-microcode

Agora vamos checar se voce esta de fato imune contra Spectre/Meltdown, reinicie sua maquina

```
curl -L https://meltdown.ovh -o spectre-meltdown-checker.sh
chmod +x spectre-meltdown-checker.sh
sudo ./spectre-meltdown-checker.sh
```

E verifique que esta tudo verde (ok).

## USBGuard para se proteger contra ataques badUSB (rubber ducky):

Ataques badUSB (mais conhecidos como rubber ducky) são aqueles onde um dispositivo eh conectado na porta USB e ele atua como um teclado injetando teclas no computador da vitima para instalar algo malicioso.

Para mitigar esses ataques vamos instalar e configurar o usb, que  

## Firejail

## MAC randômicos 

## Habilitar firewall (ufw)

## Configurações sysctl

## Apparmor

## Parâmetros de boot

## Desabilitar serviços potencialmente inseguros

## Adendum 
